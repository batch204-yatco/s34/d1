// require directive is used to load the express module/packages
// allows us to access methods and functions in easily creating our server
const express = require("express");

// This creates an express application and stores this in a constant called app
// In layman's term, app is our server
const app = express();

// Setup for allowing server to handle data from requests
// Methods used express JS are middleware
	// Middleware is software that provides common services to apps outsidde of what's offered package
app.use(express.json()); //Allows app to read and send json data

// For our app server to run, we need a port to listen to
const port = 3000;

// Express has methods corresponding to each HTTP method
// "/" corresponds with our base URI
// localhost:3000/
// Syntax: app.httpMethod("/endpoint", (req, res))
app.get("/", (req, res) => {
	// res.send uses express JS module's method to send response back to client
	res.send("Hello World");
});

// This route expects to receive a POST request at the URI/endpoint "/hello"

app.post("/hello", (req, res) => {

	// req.body contains contents/data of request
	console.log(req.body);

	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);

})

/* SCENARIO:

		We want to create a simple users database that will perform CRUD operations based on the client request. The following routes should peform its functionality:
*/

	// mock database for our users
	let users = [
		{
			username: "janedoe",
			password: "jane123"
		},
		{
			username: "johnsmith",
			password: "john123"
		}
	];

	// This route expects to receive a POST request at the URI "/signup"
	// This will create a user object in the "users" array that mirrors a real world registration:
		// All fields are required

	app.post("/signup", (req, res) => {

		console.log(req.body); 

		// If contents of "req.body" with property of "username" and "password" is not empty
		if(req.body.username !== "" && req.body.password !== "" && req.body.username !== undefined && req.body.password !== undefined){

			// To store the req.body sent via Postman in the "users" array, we will use "push" method
			// user object will be saved in "users" array 
			users.push(req.body);

			res.send(`User ${req.body.username} successfully registered`);
		}

		// If username and password are not complete
		else{
			res.send("Please input BOTH username and password");
		}

	});

	// This route expects to receive a GET request at the URI "/users"
	// This will retrieve all users stored in varaible created above
	app.get("/users", (req, res) => {
		res.send(users);
	});

	// This route expects to receive a PUT request at the URI "/change-password"
	// This will update password of a user that matches the info provided in the client/Postman
		// We will use "username" as the search property 
	app.put("/change-password", (req, res) => {

		// Creates a variable to store the message to be sent back to client/Postman(response)
		let message;

		// Create for loop that will loop through elements of the "users" array
		for(let i=0; i < users.length; i++){

			// If username provided in the client/Postman and username of current element/object in the loop is the same 
			if(users[i].username === req.body.username){
				// Changes password of the user found by the loop into the password provided in the client/postman
				users[i].password = req.body.password;

				message = `User ${req.body.username}'s password has been updated`

				// Break out of loop once user that matches the username provided in the client/Postman is found
				break;
			}
			// If no user was found
			else{
				// Changes message to be sent back as response
				message = "User does not exist!";
			}
		}

		// Sends response back to client/postman once password has been updated or if user is not found
		res.send(message);
	})	

	// This route expects to receive a DELETE request at the URI "/delete-user"
	// This will remove the user from the array for deletion
	app.delete("/delete-user", (req, res) => {

		// Creates a variable to store the message to be sent back to client/Postman(response)
		let message;

		// Create for loop that will loop through elements of the "users" array
		for(let i=0; i < users.length; i++){

			// If username provided in the client/Postman and username of current element/object in the loop is the same 
			if(users[i].username === req.body.username){

				// The splice method manipulates the array and removes user object from "users" array based on it's index
				users.splice(users[i], 1)

				message = `User ${req.body.username} has been deleted`

				// Break out of loop once user that matches the username provided in the client/Postman is found
				break;
			}
			// If no user was found
			else{
				// Changes message to be sent back as response
				message = "User does not exist!";
			}
		}

		// Sends response back to client/postman once password has been updated or if user is not found
		res.send(message);
	})	


// Returns message to confirm that the server is running in the terminal
app.listen(port, () => console.log(`Server is running at port: ${port}`));